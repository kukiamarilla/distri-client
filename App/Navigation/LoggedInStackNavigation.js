import { StackNavigator } from 'react-navigation'
import LibretaVacunacionScreen from '../Containers/LibretaVacunacionScreen'

import styles from './Styles/NavigationStyles'

export default StackNavigator(

	{
		LibretaVacunacion: {
			screen: LibretaVacunacionScreen
		}
	},
	{
		navigationOptions: {
			headerTitle: "Tus Vacunas"	    
		}
	}

)