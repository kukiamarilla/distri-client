import { SwitchNavigator } from 'react-navigation'
import LoggedInStackNavigation from './LoggedInStackNavigation'
import NotLoggedInStackNavigation from './NotLoggedInStackNavigation'

export default (loggedIn = false) => {
	return SwitchNavigator(
		{
			LoggedIn: {
				screen: LoggedInStackNavigation
			},
			NotLoggedIn: {
				screen: NotLoggedInStackNavigation
			}
		},
		{
			initialRouteName: loggedIn ? "LoggedIn" : "NotLoggedIn"
		}
	)
}