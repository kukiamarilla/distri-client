import { StackNavigator } from 'react-navigation'
import LoginScreen from '../Containers/LoginScreen'
import FailedLogin from '../Containers/FailedLoginScreen'
import styles from './Styles/NavigationStyles.js'

export default StackNavigator(
	{
		Login: {
			screen: LoginScreen
		},
		FailedLogin: {
			screen: FailedLogin
		}
	},
	{
		headerMode: 'none',
		navigationOptions: {
		    headerStyle: styles.header
		}
	}
)