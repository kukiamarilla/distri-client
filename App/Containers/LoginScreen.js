import React, { Component} from 'react'
import { Text, View, TextInput } from 'react-native'
import RoundedButton from '../Components/RoundedButton'
import { StackNavigator } from 'react-navigation'
import { login } from '../Services/Authentication'
import Api from '../Services/Api'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'

export default class LoginScreen extends Component {

  constructor(props){
    super(props)
    this.username = null
    this.password = null
  }

  handleChangeUsername = (text) => {
    this.username = text 
  }

  handleChangePassword = (text) => {
    this.password = text 
  }

  handleLogin = () => {
    Api().getToken(this.username, this.password)
      .then(response => response.json())
      .then(res => {
        console.log(res)
        if(res.error !== undefined){
          console.log(res.error)
          this.props.navigation.navigate('FailedLogin')
        }else{
          navigation = this.props.navigation
          login(res.access_token).then(() => {navigation.navigate('LoggedIn')})
        }
      })
      .catch(err => this.props.navigation.navigate('FailedLogin'))
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <View style={styles.well}>
            <Text style={styles.text}>Inicio de Sesión</Text>
            <TextInput 
            placeholder="Usuario"
            onChangeText={this.handleChangeUsername}
            style={styles.input}
            />
            <TextInput 
            placeholder="Contraseña" 
            onChangeText={this.handleChangePassword}
            style={styles.input}
            secureTextEntry={true}
            />
            <RoundedButton 
              text="Iniciar sesión"
              style={styles.button}
              onPress={this.handleLogin}
            />
          </View>
        </View>
      </View>
    )
  }
}
