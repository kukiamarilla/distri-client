import React, { Component } from 'react'
import { Text, View, TextInput, AsyncStorage, ActivityIndicator, ScrollView, ListView } from 'react-native'
import FullButton from '../Components/FullButton'
import { StackNavigator } from 'react-navigation'
import { logout, getOAuthToken } from '../Services/Authentication'
import Api from '../Services/Api'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LibretaVacunacionScreenStyle'
import Fonts from '../Themes/Fonts'

export default class LibretaVacunacionScreen extends Component {
  
  
  constructor(props){
    super(props)
    this.navigation = this.props.navigation
    this.state = {
      isLoading: true,
      vacunas: []
    }
  }

  componentDidMount(){
    try{
      getOAuthToken()
        .then(token => {
          fetch('http://172.20.10.5:8000/api/vacunas',{
            method: 'GET',
            headers:{
              Accept: 'application/json',
              'Content-Type': 'application/json;charset=UTF-8',
              Authorization: 'Bearer ' + token
            }
          })
            .then(response => response.json())
            .then(responseJson => {
              const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
              this.setState({
                isLoading: false,

                vacunas: ds.cloneWithRows(responseJson)
              })

            })
        })
    }catch(err){
      console.log(err)
    }
  }

  handleLogout = () => {
    logout()
    this.props.navigation.navigate("NotLoggedIn")
  }

  renderRow(rowData, sectionID ){
      return (
        <View style={styles.vacuna}>
          <Text style={Fonts.style.h5}>{rowData.vacuna}</Text>
          <Text>{rowData.created_at}</Text>
        </View>
      )
  }

  render () {
     if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

   
    return (
      <ScrollView style={styles.mainContainer}>
        <View style={styles.container}>
           <ListView
              dataSource={this.state.vacunas}
              renderRow={this.renderRow}
            />
            <FullButton
              text = "Cerrar Sesión"
              onPress = {this.handleLogout}
              style={styles.button}
            />
        </View>
      </ScrollView>
    )
  }
}
