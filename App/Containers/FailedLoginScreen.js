import React, { Component} from 'react'
import { Text, View, TextInput, Button } from 'react-native'
import RoundedButton from '../Components/RoundedButton'
import { StackNavigator } from 'react-navigation'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'

export default class FailedLoginScreen extends Component {

  render () {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.actionBar}></View>
        <View style={styles.container}>
          <View style={styles.well}>
            <Text style={styles.text}>No se ha podido iniciar sesión</Text>
            <Text>Vuelva a intentarlo</Text>
            <RoundedButton 
              text="Volver"
              style={styles.button}
              onPress={() => this.props.navigation.navigate("Login")}/>
          </View>
        </View>
      </View>
    )
  }
}
