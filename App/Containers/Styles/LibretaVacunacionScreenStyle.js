import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  mainContainer: {
    backgroundColor: Colors.background
  },
  vacuna: {
    backgroundColor: Colors.snow,
    height: 80,
    margin: 10,
    borderRadius: 10,
    padding: 10
  },
  
})
