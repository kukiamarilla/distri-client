import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  container: {
    flex: 1,
    backgroundColor: Colors.background,
  	padding: 10,
  	justifyContent: 'center',
  	alignItems: 'center'

  },
  text:{
  	fontSize: Fonts.size.h4
  },
  well:{
  	backgroundColor: Colors.snow,
  	width: Metrics.screenWidth *3/4,
  	height: Metrics.screenHeight*2/5,
  	borderRadius: 10,
  	paddingHorizontal: 50,
  	paddingVertical: 30
  },
  input: {
  	width: Metrics.screenWidth * 3/4 - 100,
  	marginTop: 20,
  },
  button:{
  	marginTop:40
  },
  link:{
    color: Colors.charcoal,
    backgroundColor: Colors.snow
  }
})
