import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import Navigation from '../Navigation/AppNavigation'
import { isLoggedIn } from '../Services/Authentication'
import { login } from '../Services/Authentication'

// Styles
import styles from './Styles/RootContainerStyles'

export default class RootContainer extends Component {
 	
	constructor(props) {
		super(props);

		this.state = {
			loggedIn: false,
			checkedLoggedIn: false
		};
	}
	componentDidMount() {
		isLoggedIn()
			.then(res => this.setState({ loggedIn: res, checkedLoggedIn: true }))
			.catch(err => login(null));
	}

	render () {
		const {loggedIn, checkedLoggedIn} = this.state

		const Nav = Navigation(loggedIn);
		return (
			<View style={styles.applicationView}>
				<StatusBar barStyle='light-content' />
				<Nav />
			</View>
		)
	}
}



