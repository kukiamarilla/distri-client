import { AsyncStorage } from 'react-native'

export const login = (token) => AsyncStorage.setItem('USER_TOKEN', token)
	
export const logout = () => AsyncStorage.removeItem('USER_TOKEN')

export const isLoggedIn = () => {

	return new Promise((resolve, reject) => {
		
		AsyncStorage.getItem('USER_TOKEN')
			.then(res => {
				if(res !==null){
					return resolve(true)
				}else{
					return resolve(false)
				}
			})
			.catch(err => reject(err))
	
	})
}

export const getOAuthToken = () => {
	return new Promise((resolve, reject) => {
		
		AsyncStorage.getItem('USER_TOKEN')
			.then(res => {
				if(res !==null){
					return resolve(res)
				}else{
					return resolve(false)
				}
			})
			.catch(err => reject(err))
	
	})
}