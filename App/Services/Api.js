import { isLoggedIn, getOAuthToken } from './Authentication'

export default Api = () => {
	const URL = "http://172.20.10.5:8000/"

	var headers = {
		Accept: 'application/json',
		'Content-Type': 'application/json;charset=UTF-8'
	}
	const setHeader = (header) => {
		headers = Object.assign(headers, header)
		
	}
	const getToken = (username, password) => {
		return fetch(URL+'oauth/token',{
			method: 'POST',
			headers: headers,
			body: JSON.stringify({
				grant_type: 'password',
				client_id: 5,
				client_secret: 'm44dssxcbPEICA6Ume2cNdlJq1CrlfEPRxuNOh8x',
				username: username,
				password: password
			})
		})
	}
	const getLibreta = () => fetch(URL+'api/vacunas', {headers: headers})
	const getUser = () => fetch(URL+'api/user', {headers: headers})

	return {
		setHeader,
		getToken,
		getLibreta,
		getUser
	}
}